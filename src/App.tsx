import styles from './App.module.css';

function App() {
  return (
    <div className='size-full flex justify-center'>
      <h1 className={`${styles.fadeInHi} ${styles.shadow} text-center`}>Hi</h1>
      <h1 className={`${styles.fadeInComma} ${styles.shadow} text-center`}>
        ,&nbsp;
      </h1>
      <h1 className={`${styles.fadeInRest} ${styles.shadow} text-center`}>
        I'm Dan.
      </h1>
    </div>
  );
}

export default App;
